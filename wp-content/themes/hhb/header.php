<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Hornhautbank München
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Hornhautbank München">
    <meta name="author" content="Hornhautbank München">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap Core -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <!-- <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script> -->
    <script src="<?php bloginfo('template_url'); ?>/js/modernizr.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.ba-throttle-debounce.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.touchswipe.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.caroufredsel.min.js"></script>
    <?php wp_head(); ?>

    <script>
        $(document).ready(function(){

            var navigationElem = $('.secondary-navigation > div');
            if( navigationElem && navigationElem.length ) {
                var repositioning = false,
                    scrollTopPos = 0;

                setInterval(function () {
                    var offset = $(document).height() - navigationElem.offset().top -
                                    navigationElem.height();

                    if( !repositioning && offset < 220 ) {
                        repositioning = true;
                        scrollTopPos = $(window).scrollTop();

                        navigationElem.removeClass('affix').css({
                            top: $(document).height() - $('#page > footer').height() -
                                    navigationElem.height() - $('#page > header').height() - 95,
                            position: 'absolute'
                        });
                    } else if( repositioning && $(window).scrollTop() < scrollTopPos ) {
                        repositioning = false;
                        navigationElem.attr('style', '');

                        // css({ position: 'fixed', top: 60 })
                        navigationElem.addClass('affix');
                    }
                }, 200);
            }

            if( $('.mod-csstransforms3d') && $('.flip-container') && $('.flip-container').length > 0 ) {
                var flipInt = setInterval( function() { $('.flip-container').toggleClass('hover'); }, 3000 );
                $('.flip-container').css('cursor', 'pointer').on('click', function(e) {
                    clearInterval(flipInt);
                    $('.flip-container').toggleClass('hover');
                    flipInt = setInterval( function() { $('.flip-container').toggleClass('hover'); }, 3000 );
                });
            }



        });
        $(window).load(function() {
            if( $('#slider-items').length ) {
                $('#slider-items').carouFredSel({
                    items: {
                        visible: {
                            min: 1,
                            max: 1
                        },
                        minimum: 1,
                        width: '100%'
                    },
                    scroll: 1,
                    auto: 5000,
                    onCreate: function(data) {

                    }
                });

                $('.slider-arrow').on('click', function(e) {
                    e.preventDefault();
                    if( $(this).hasClass('arrow-left') ) {
                        $('#slider-items').trigger('prev', 1);
                    } else {
                        $('#slider-items').trigger('next', 1);
                    }
                });
            }
        });
    </script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>

    <header>
        <div class="container logobar">
            <div class="row">
                <div class="col-md-5 col-sm-6 logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url'); ?>/images/hornhautbank-muenchen-logo.png" alt="Hornhautbank München"/></a>
                </div>
                <div class="col-md-3 col-sm-6 tools hidden-xs hidden-sm">
                    <ul>
                        <li class="wp-font-size"><a href="#small" id="wpFontMinus-2" class="font-adjust-sm">Aa</a></li>
                        <li class="wp-font-size"><a href="#medium" id="wpFontMinus-3" class="font-adjust-md">Aa</a></li>
                        <li class="wp-font-size"><a href="#large" id="wpFontMinus-4" class="font-adjust-lg">Aa</a></li>
                        <li><a  target="_blank" href="<?php echo get_permalink(371); ?>"><img src="<?php bloginfo('template_url'); ?>/images/flag-en.png" alt="English version"/></a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 meta-navigation hidden-xs">
                    <?php
                    wp_nav_menu(
                        array(
                            'menu' => 'navigation-meta',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'container' => false,
                        )
                    );
                    ?>
                    <ul class="visible-sm visible-xs">
                        <li><a href="<?php echo get_permalink(371); ?>"><img src="<?php bloginfo('template_url'); ?>/images/flag-en.png" alt="English version"/></a></li>
                    </ul>
                    <div class="button-login">
                        <a class="btn btn-default" target="_blank" href="https://www.datenbank.hornhautbank-muenchen.de/"><i class="glyphicon glyphicon-log-in"></i> für Augenbanken und Ärzte</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="jumbotron navline">
            <div class="container navigation">
                <div class="row">
                    <div class="col-md-9">
                        <?php

                        /**
                         * Das Menü wird mittels Design ->> Menüs verwaltet und erstellt
                         * Neue Seiten werden nicht automatisch ausgegeben.
                         * -
                         * Das Sub-Menü innerhalb des Content-Bereichs verhält sich hingegen anders
                         */
                        $defaults = array(
                            'menu'            => 'navigation-primary',
                            'menu_class'      => 'navigation-primary clearfix',
                            'container'       => false,
                            'echo'            => true,
                            'depth'           => 2,
                            'walker'          => new Fresh_Menu_Walker(),
                        );
                        wp_nav_menu( $defaults );
                        ?>
                    </div>
                    <div class="col-md-3 search">
                        <div class="hidden-xs">
                            <?php get_search_form(); ?>
                        </div>
                        <div class="visible-xs">
                            <?php
                            wp_nav_menu(
                                array(
                                    'menu' => 'navigation-meta',
                                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'container' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <p class="visible-xs text-center"><a href="<?php echo get_permalink(371); ?>"><img src="<?php bloginfo('template_url'); ?>/images/flag-en.png" alt="English version"/></a> <a class="btn btn-default" style="color: #BBB;" href="https://www.datenbank.hornhautbank-muenchen.de/"><i class="glyphicon glyphicon-log-in"></i> für Augenbanken und Ärzte</a></p>
                </div>
            </div>
        </div>
    </header>
