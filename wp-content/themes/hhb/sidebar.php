<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Hornhautbank München
 */

if( is_active_sidebar('sidebar-1') && !is_front_page() ):
?>
<div class="col-md-3 contentbar">
    <?php
    // Initialisiert die Sidebar
    dynamic_sidebar( 'sidebar-1' );
    ?>
    <div class="contentbar-after">
    <p><img class="img-left" src="<?php bloginfo('template_url'); ?>/images/logo-tuev-sued.png" alt="TÜV Süd geprüft"> <span>Seit 05.11.2006<br> ISO 9001 zertifiziert.</span></p>
    </div>
</div>
<?php
elseif( is_front_page() || is_home() ):
?>
<div class="col-md-3 contentbar sliderbar">
    <div id="gallery-slider" class="gallery-slider">
        <h4>Impressionen</h4>
        <div class="slider">
            <div class="slider-arrow arrow-left" style=""></div>
            <div class="slider-arrow arrow-right" style=""></div>
            <div id="slider-items">
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/7.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/7-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/6.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/6-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/4.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/4-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/3.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/3-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/2.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/2-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/1.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/1-thumb.jpg" alt=""/></a>
                </div>
                <div class="slider-item">
                    <a rel="lightbox-0" href="<?php bloginfo('template_url'); ?>/images/slider/5.jpg"><img src="<?php bloginfo('template_url'); ?>/images/slider/5-thumb.jpg" alt=""/></a>
                </div>
            </div>
        </div>
    </div>
    <div class="contentbar-after">
        <p><img class="img-left" src="<?php bloginfo('template_url'); ?>/images/logo-tuev-sued.png" alt="TÜV Süd geprüft"> <span>Seit 05.11.2006<br> ISO 9001 zertifiziert.</span></p>
    </div>
</div>
<?php endif; ?>
