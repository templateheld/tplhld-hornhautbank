<?php
/**
 * The template for displaying search forms in Hornhautbank München
 *
 * @package Hornhautbank München
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Suche', 'placeholder', 'hhb' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
        <span class="input-group-btn">
            <input type="submit" class="search-submit btn btn-default" value="<?php echo esc_attr_x( 'Suche', 'submit button', 'hhb' ); ?>">
        </span>
    </div>
</form>