<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Hornhautbank München
 */

get_header(); ?>

<div class="container contentpage">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="contentstage search">
                <?php if ( have_posts() ) : ?>
                    <header>
                        <h1 class="search-title"><i class="glyphicon glyphicon-search"></i> <?php printf( __( 'Suchergebnisse für %s', 'hhb' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                    </header>
                    <hr/>
                    <section>
                        <h2>Neue Suche</h2>
                        <?php get_search_form(); ?>
                    </section>
                    <hr/>
                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'content', 'search' ); ?>
                    <?php endwhile; ?>

                    <?php hhb_paging_nav(); ?>

                <?php else : ?>

                    <?php get_template_part( 'content', 'none' ); ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
