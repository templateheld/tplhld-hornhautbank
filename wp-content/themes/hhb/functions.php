<?php
/**
 * Hornhautbank München functions and definitions
 *
 * @package Hornhautbank München
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'hhb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function hhb_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Hornhautbank München, use a find and replace
	 * to change 'hhb' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'hhb', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'hhb' ),
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'hhb_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // hhb_setup
add_action( 'after_setup_theme', 'hhb_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function hhb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Standard-Sidebar', 'hhb' ),
		'id'            => 'sidebar-1',
        'description'   => 'Global',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	));

    /*
    register_sidebar( array(
        'name'          => __( 'Standard-Sidebar: Startseite', 'hhb' ),
        'id'            => 'sidebar-home',
        'description'   => 'Standard-Sidebar Startseite',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span class="hidden">',
        'after_title'   => '</span>',
    ));
    */

}
add_action( 'widgets_init', 'hhb_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hhb_scripts() {
	wp_enqueue_style( 'hhb-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'hhb-cookie-style', get_template_directory_uri() . '/css/cookieconsent.min.css' );
	wp_enqueue_style( 'hhb-custom-style', get_template_directory_uri() . '/css/hhb-custom.css' );
	wp_enqueue_style( 'hhb-style', get_stylesheet_uri() );

	wp_enqueue_script( 'hhb-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'hhb-cookie-js', get_template_directory_uri() . '/js/cookieconsent.min.js', array(), '', true );

	wp_enqueue_script( 'hhb-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hhb_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Allow custom file mimes in media upload
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
