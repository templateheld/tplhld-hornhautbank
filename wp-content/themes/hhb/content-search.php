<?php
/**
 * @package Hornhautbank München
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><i class="glyphicon glyphicon-circle-arrow-right"></i> <?php the_title(); ?></a></h2>
        <p class="search-url"><small><?php the_permalink(); ?></small></p>
	</header>
    <section>
        <div class="entry-summary">
            <?php the_excerpt(); ?>
        </div>
        <a href="<?php the_permalink(); ?>"></a>
    </section>
    <hr/>
</article>