<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Hornhautbank München
 */

get_header(); ?>

<div class="container contentpage">
    <div class="row">
        <div class="col-md-3 col-sm-4 secondary-navigation">
            <!-- EMPTY -->
        </div>
        <div class="col-md-<?php echo ( !is_active_sidebar('sidebar-1') ? '9' : '6' ) ?> col-sm-8">
            <div class="contentstage">
                <section class="error-404 not-found">
                    <header class="page-header">
                        <h1 class="page-title"><?php _e( 'Fehler 404 - Seite nicht gefunden', 'hhb' ); ?></h1>
                    </header><!-- .page-header -->

                    <div class="page-content">
                        <p><?php _e( 'Leider konnten wir die von Ihnen aufgerufene Seite nicht finden. Sie können jedoch unsere Suchfunktion nutzen, um gezielt nach Inhalten zu suchen.', 'hhb' ); ?></p>

                        <hr/>
                        <?php get_search_form(); ?>

                    </div><!-- .page-content -->
                </section><!-- .error-404 -->
            </div>
        </div>
        <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer(); ?>