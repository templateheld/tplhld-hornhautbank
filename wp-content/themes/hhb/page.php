<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Hornhautbank München
 */

get_header(); ?>

    <div class="container contentpage">
        <div class="row">
            <div class="col-md-3 col-sm-4 secondary-navigation">
                <div data-spy="affix" data-offset-top="220">
                    <?php

                    $defs = array(
                        'child_of'     => 0,
                        'date_format'  => get_option('date_format'),
                        'depth'        => 2,
                        'echo'         => 0,
                        'post_type'    => 'page',
                        'post_status'  => 'publish',
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'title_li'     => '',
                        'walker'       => new Fresh_Page_Walker(),
                    );

                    $children = false;
                    if( !$post->post_parent ){
                        $defs['child_of'] = $post->ID;
                        // will display the subpages of this top level page
                        $children = wp_list_pages( $defs );
                    } else {
                        if( $post->ancestors ) {
                            // now you can get the the top ID of this page
                            // wp is putting the ids DESC, thats why the top level ID is the last one
                            $navPage = $post->ancestors[count($post->ancestors)-1];
                            $defs['child_of'] = $navPage;
                            $children = wp_list_pages( $defs );
                        }
                    }

                    if( $children ) { ?>
                        <ul>
                            <?php echo $children; ?>
                        </ul>
                        <div class="affix-after">
                            <p><a href="<?php echo get_permalink(16); ?>"><img style="width: 115px;" class="img-left" src="<?php bloginfo('template_url'); ?>/images/organspendeausweis.png" alt="Organspendeausweis"/><strong>Organspende</strong></a><br/>
                                Jetzt entscheiden und Ausweis<br/>
                                beantragen.</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-<?php echo ( !is_active_sidebar('sidebar-1') ? '9' : '6' ) ?> col-sm-8">
                <div class="contentstage">
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'content', 'page' ); ?>

                    <?php endwhile; // end of the loop. ?>
                </div>
            </div>

            <?php get_sidebar(); ?>

        </div>
    </div>

<?php get_footer(); ?>
