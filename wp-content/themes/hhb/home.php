<?php
/**
 * Template Name: Startseite
 */
get_header();
?>

    <div class="jumbotron home">
        <div class="container">
            <div class="col-md-6 col-md-offset-6">
                <h1>Sag ja zum Leben!<br/>
                    <span>Ihre Hilfe wird benötigt.</span></h1>
                <p><a class="btn btn-primary btn-lg btn-black" role="button" href="<?php echo get_permalink(31); ?>">jetzt entscheiden</a></p>
            </div>
        </div>
    </div>

    <div class="container teaser">
        <div class="row stage">
            <div class="col-md-<?php echo ( !is_active_sidebar('sidebar-1') ? '12' : '9' ) ?>">
                <div class="contentstage start">
                    <?php the_post(); the_content(); ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>

<?php
get_footer();