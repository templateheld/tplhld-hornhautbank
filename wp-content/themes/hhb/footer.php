<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Hornhautbank München
 */
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="supported">
                    <a href="http://www.neuhann.de/" target="_blank"><img src="/wp-content/uploads/logo-mvz.png" alt="Praxis Neuhann MVZ"/></a>
                    <a href="http://www.gutsehen.de/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/logo-euroeyes.jpg" alt="Euroeyes"/></a>
                    <a href="http://ocunet.de/ocunet/muenchen.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/logo-ocunet.jpg" alt="Ocunet"/></a>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron footer">
        <div class="container">
            <div class="row visible-xs search-mobile">
                <div class="col-sm-12">
                    <p><strong>Stichwortsuche</strong></p>
                    <?php get_search_form(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p><strong>Hornhautbank München</strong><br>
                        Gemeinnützige GmbH<br>
                        Hans-Stützle-Str. 21<br>
                        81249 München</p>
                </div>
                <div class="col-sm-3">
                    <p>&nbsp;<br>
                        Telefon	089 13 29 10<br>
                        Telefax	089 13 29 11<br>
                        info@hornhautbank-muenchen.de</p>
                </div>
                <div class="col-sm-3">
                    <p><a href="<?php echo get_permalink(16); ?>"><img class="img-left" src="<?php bloginfo('template_url'); ?>/images/organspendeausweis.png" alt="Organspendeausweis"/><strong>Organspende</strong><br/>
                        Jetzt entscheiden und Ausweis<br/>
                        beantragen.</a></p>
                </div>
                <div class="col-sm-3">
                    <?php
                    wp_nav_menu(
                        array(
                            'menu' => 'navigation-service',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'container' => false,
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>© <?php echo date('Y'); ?> Hornhautbank München | Alle Rechte vorbehalten | concept &amp; design by <a href="http://www.freshframes.com" target="_blank">fresh frames</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<!-- Google Analytics -->
<script>
    // Set to the same value as the web property used on the site
    var gaProperty = 'UA-48005061-1';

    // Disable tracking if the opt-out cookie exists.
    var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
        window[disableStr] = true;
    }

    // Opt-out function
    function gaOptout() {
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
        window[disableStr] = true;
        alert('Sie werden nun auf dieser Webseite nicht mehr über Google Analytics erfasst.');
    }

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', gaProperty, 'hornhautbank-muenchen.de');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

</script>

<!-- Cookie Consent -->
<script>
  <?php $privacy_link = get_permalink(422); ?>

  window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#000"
        },
        "button": {
          "background": "#f1d600"
        }
      },
      'content': {
        'message': 'Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies setzen.',
        'dismiss': 'VERSTANDEN',
        'link': 'Weitere Informationen zu Cookies und Datenschutz finden Sie hier.',
        'href': '<?php echo $privacy_link ?>'
      }
    })
  });

</script>

</body>
</html>
