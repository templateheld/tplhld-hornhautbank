<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hornhautbank' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?fJZVUg16R+Yy5};yV<?Y`J{ye5]::$5Q}Sc2D7p#Lfg!lJsOb% NWn}bd%u>x&i' );
define( 'SECURE_AUTH_KEY',  '+Dld5rw+]^Iw.9KX^%_4o![*2bVzqu~6GNSn:invo~-GVl2V1_yr_~cFxm_L5Fb,' );
define( 'LOGGED_IN_KEY',    '_6_gr_L#W^w@}-#6`[<${Z^wK>3;csy:;E=H<xx;rYU K`Gjy70a7Eut6X-a_F@5' );
define( 'NONCE_KEY',        'U[>P+1.v{`#O `O9;RTaLfy>KGIf%5+x8wo:%QQWR?4#tqNdpT>YIhn#jcmiYJYM' );
define( 'AUTH_SALT',        '@9=I1vY?cM~m@_f4G&a!%,U^u0bHMp+tWWqJ%`z^`i!F]T#W{Hcz1`5NrB+*HWW]' );
define( 'SECURE_AUTH_SALT', 'u>EGxa2:?%IH=7:@B,l,s2{*N(P{6~/dlnztgh13 T,ak<zxXkc=oK=$Mg/upA^5' );
define( 'LOGGED_IN_SALT',   'n*z8f+#AsEg1Pka(5LcrN(9F`_JH/`%e[ny?=e^PWtj.`@qS%I>zQV[A93^Z[L)9' );
define( 'NONCE_SALT',       ',*Ox^lhpbu0kD~hdtLLZ.1f{b?!yI&sVfNc3>]l`lkfOzd24/DA9};,}47jN2UdY' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
